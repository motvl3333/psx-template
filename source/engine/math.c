#include <engine/math.h>

int abs(int num)
{
	if(num < 0)
	{
		return -(num);
	}

	return num;
}

// square an integer representing a floating-point number
unsigned int fsquare(unsigned int num)
{
	return num*num/ONE;
}

// use binary search to find rough estimate
// of square root of integers (representing
// floating-point) between 0 and ((1<<32)-1).
// (0.0f - 1048576.0f).
unsigned int fsqrt(unsigned int num)
{
#define MAX_ITERATIONS	24

	unsigned char		i;
	unsigned short		max_guess, guess, delta;
	unsigned int		estimate = 0;

	// prevent overflow, since 65536^2 == 2^32, which is greater
	// than UINT_MAX.
	max_guess = num > USHRT_MAX ? USHRT_MAX : num;

	guess = max_guess/2;
	delta = max_guess/4;

	for(i = 0; i < MAX_ITERATIONS; i++)
	{
		estimate = guess*guess;

		if(estimate > num)
		{
			guess -= delta;
		}
		else if(estimate < num)
		{
			guess += delta;
		}
		else
		{
			return guess*ONE_SQRT;
		}

		delta /= 2;
	}

	return guess*ONE_SQRT;
}

