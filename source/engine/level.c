#include <engine/level.h>
#include <engine/entity.h>
#include <engine/display.h>

char init_section(struct Section *p_section, unsigned char (*p_map)[MAX_COLS], struct Vector2x *p_offset, struct Vector2w *p_sprite_size, unsigned short clut, DR_TPAGE *dr_tpage)
{
	int		i, j, row = 0, col = 0;
	SPRT		*p_sprt; // for brevity

	// set the map
	p_section->p_current_map = p_map;

	// set the offset
	p_section->offset.x = p_offset->x;
	p_section->offset.y = p_offset->y;

	// assign tileset uv values in 256x256px texture (tile #0 is air)
	for(i = 1; i < UNIQUE_TILES; i++)
	{
		p_section->tileset[i].u = col * (TILE_SPRITE_SIZE + TILE_SPACING);
		p_section->tileset[i].v = row * (TILE_SPRITE_SIZE + TILE_SPACING);

		col++;

		// there are 15 16x16px tiles in each row of a 256x256px tileset (with 1px TILE_SPACING)
		if( !(i%15) )
		{
			row++;
			col = 0;
		}
	}

	// init every SPRT of p_sprite_layout
	for(row = 0; row < MAX_ROWS; row++)
	{
		for(col = 0; col < MAX_COLS; col++)
		{
			// set current sprite
			p_sprt = &p_section->p_sprite_layout[row][col];

			SetSprt(p_sprt);

			// set RGB of each sprite
			setRGB0(p_sprt, 128, 128, 128);

			// set (w, h) of each sprite
			p_sprt->w = p_sprite_size->w;
			p_sprt->h = p_sprite_size->h;

			// set (x, y) of each sprite
			p_sprt->x0 = col * p_sprt->w + p_section->offset.x;
			p_sprt->y0 = row * p_sprt->h + p_section->offset.y;

			// set (u, v) of each sprite
			p_sprt->u0 = p_section->tileset[p_map[row][col]].u;
			p_sprt->v0 = p_section->tileset[p_map[row][col]].v;

			// set clut of each sprite
			p_sprt->clut = clut;
		}
	}

	// set the DR_TPAGE primitive
	p_section->dr_tpage = *dr_tpage;

	return 1;
}

// draw_section() draws the relevant tiles around the camera.
void draw_section(struct Section *p_section, struct Camera *p_camera)
{
	int		row, col;
	int		leftmost_tile, topmost_tile;

	// at any given time, only 24x18 tiles are rendered on-screen.
	// these values can be changed via the TILES_RENDERED_HORIZONTALLY
	// and TILES_RENDERED_VERTICALLY #defines in level.h
	leftmost_tile = p_camera->global_tile_pos.x;
	topmost_tile  = p_camera->global_tile_pos.y;

	leftmost_tile -= TILES_RENDERED_HORIZONTALLY/2;
	topmost_tile  -= TILES_RENDERED_VERTICALLY/2;

	// prevent buffer overflow for horizontal rendering
	if(leftmost_tile < RENDER_LEFT_SIDE_LIMIT)
	{
		leftmost_tile = RENDER_LEFT_SIDE_LIMIT;
	}
	else if(leftmost_tile > RENDER_RIGHT_SIDE_LIMIT)
	{
		leftmost_tile = RENDER_RIGHT_SIDE_LIMIT;
	}
	// prevent buffer overflow for vertical rendering
	if(topmost_tile < RENDER_TOP_SIDE_LIMIT)
	{
		topmost_tile = RENDER_TOP_SIDE_LIMIT;
	}
	else if(topmost_tile > RENDER_BOTTOM_SIDE_LIMIT)
	{
		topmost_tile = RENDER_BOTTOM_SIDE_LIMIT;
	}

	// since SPRTs do not have their own tpage member, we must draw the DR_TPAGE primitive first
	// before drawing any sprites.
	DrawPrim(&p_section->dr_tpage);

	// render the level last
	for(row = 0; row < TILES_RENDERED_VERTICALLY; row++)
	{
		for(col = 0; col < TILES_RENDERED_HORIZONTALLY; col++)
		{
			// don't render air tiles
			if(p_section->p_current_map[topmost_tile + row][leftmost_tile + col] == AIR_TILE)
			{
				continue;
			}

			DrawPrim(&p_section->p_sprite_layout[topmost_tile + row][leftmost_tile + col]);
		}
	}
}

// every tile including air tiles are to be moved
void move_section(struct Section *p_section, struct Camera *p_camera)
{
	int			row, col;
	short			leftmost_tile, topmost_tile; // indexes to p_sprite_layout

	// (x, y) positions of specified tiles
	short			leftmost_tile_pos_x;
	short			topmost_tile_pos_y;

	SPRT			*p_sprt; // for brevity

	// at any given time, only 24x18 tiles are rendered on-screen.
	// these values can be changed via the TILES_RENDERED_HORIZONTALLY
	// and TILES_RENDERED_VERTICALLY #defines in level.h
	leftmost_tile = p_camera->global_tile_pos.x;
	topmost_tile  = p_camera->global_tile_pos.y;

	leftmost_tile -= TILES_RENDERED_HORIZONTALLY/2;
	topmost_tile  -= TILES_RENDERED_VERTICALLY/2;

	// prevent buffer overflow for horizontal rendering
	if(leftmost_tile < RENDER_LEFT_SIDE_LIMIT)
	{
		leftmost_tile = RENDER_LEFT_SIDE_LIMIT;
	}
	else if(leftmost_tile > RENDER_RIGHT_SIDE_LIMIT)
	{
		leftmost_tile = RENDER_RIGHT_SIDE_LIMIT;
	}
	// prevent buffer overflow for vertical rendering
	if(topmost_tile < RENDER_TOP_SIDE_LIMIT)
	{
		topmost_tile = RENDER_TOP_SIDE_LIMIT;
	}
	else if(topmost_tile > RENDER_BOTTOM_SIDE_LIMIT)
	{
		topmost_tile = RENDER_BOTTOM_SIDE_LIMIT;
	}

	// calculate the leftmost tile SCREEN position
	leftmost_tile_pos_x = p_camera->center_focal_point.x - \
				(TILES_RENDERED_HORIZONTALLY*TILE_SPRITE_SIZE)/2;

	// calculate the topmost tile SCREEN position
	topmost_tile_pos_y = p_camera->center_focal_point.y - \
				(TILES_RENDERED_VERTICALLY*TILE_SPRITE_SIZE)/2;

	// move all on-screen tiles
	for(row = 0; row < TILES_RENDERED_VERTICALLY; row++)
	{
		for(col = 0; col < TILES_RENDERED_HORIZONTALLY; col++)
		{
			p_sprt = &p_section->p_sprite_layout[topmost_tile + row][leftmost_tile + col];

			// ensure the correct position for each sprite
			p_sprt->x0 = leftmost_tile_pos_x + \
					(TILE_SPRITE_SIZE * col) - (p_camera->global_pos.x % TILE_SPRITE_SIZE);
			p_sprt->y0 = topmost_tile_pos_y + \
					(TILE_SPRITE_SIZE * row) - (p_camera->global_pos.y % TILE_SPRITE_SIZE);

		}
	}
}

