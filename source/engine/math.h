#ifndef MATH_H
#define MATH_H

#include <limits.h>

// If you need higher precision than
// 1/4096, change ONE to a higher
// value. ONE_SQRT should be sqrt(ONE)
#define ONE		4096
#define ONE_SQRT	64

int abs(int num);
unsigned int fsquare(unsigned int num);
unsigned int fsqrt(unsigned int num);

#endif

