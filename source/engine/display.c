#include <engine/display.h>

void switch_double_buffer()
{
	p_cdb = (p_cdb == db) ? db+1 : db;

	PutDrawEnv(&p_cdb->draw);
	PutDispEnv(&p_cdb->disp);
}

