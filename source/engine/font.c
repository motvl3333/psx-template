#include <engine/font.h>

void print_textbox(struct TextBox *p_box)
{
	int row, col;
	
	// it is critical that we set horizontal_offset and vertical_offset to 0 here.
	// forgetting to do this leads to garbage values that screw up
	// the function.
	int horizontal_offset = 0, vertical_offset = 0; // offset in number of characters
	int index;
	int length;
	
	length = strlen(p_box->message_data);
	
	// set the tpage for all the SPRTs in the for loop
	DrawPrim(&p_box->dr_tpage);
	
	for(index = 0; index < length; index++)
	{
		if(p_box->message_data[index] == '\n')
		{
			horizontal_offset = 0;
			vertical_offset += 2;
			continue;
		}
		
		if(p_box->message_data[index] == ' ')
		{
			horizontal_offset += 1;
			continue;
		}
		
		// The specific font texture we are using has 10 rows and
		// 10 columns. It starts with '!' and ends with '~' corresponding
		// to ASCII 33 - 126. We are converting the ASCII values present in
		// message data to the corresponding row and column in the font texture.
		// This helps determine the UV values for each SPRT in the display_data 
		// array.
		col = (p_box->message_data[index] - 33)%10;
		row = (p_box->message_data[index] - 33)/10;
		
		// determine UV values
		p_box->display_data[index].u0 = p_box->u + (col * p_box->char_w); // should be an even number
		p_box->display_data[index].v0 = p_box->v + (row * p_box->char_h);
		
		// go to a new line if we've reached maximum characters horizontally
		if(horizontal_offset > p_box->max_x)
		{
			horizontal_offset = 0;
			vertical_offset += 1;
		}
		
		// apply current char position
		p_box->display_data[index].x0 = p_box->x + horizontal_offset * p_box->char_w;
		p_box->display_data[index].y0 = p_box->y + vertical_offset * p_box->char_h;
		
		horizontal_offset += 1;
		
		DrawPrim(&p_box->display_data[index]);
	}
}

void init_textbox_sprites(struct TextBox *p_box, unsigned short clut_id, unsigned short tpage_id)
{
	int i;
	
	// initialize textbox's tpage
	SetDrawTPage(&p_box->dr_tpage, 0, 0, tpage_id);
	
	// u0, v0, x0, and y0 are set in print_textbox(), not here
	for(i = 0; i < 128; i++)
	{
		SetSprt(&p_box->display_data[i]);
		
		p_box->display_data[i].w = p_box->char_w; // should be an even number
		p_box->display_data[i].h = p_box->char_h;
		
		p_box->display_data[i].clut = clut_id;
		
		p_box->display_data[i].r0 = 128;
		p_box->display_data[i].g0 = 128;
		p_box->display_data[i].b0 = 128;
	}
}

void set_text_color(struct TextBox *p_box, unsigned char red, unsigned char green, unsigned char blue)
{
	int i;
	
	for(i = 0; i < MAX_MESSAGE_LENGTH; i++)
	{
		p_box->display_data[i].r0 = red;
		p_box->display_data[i].g0 = green;
		p_box->display_data[i].b0 = blue;
	}
}

