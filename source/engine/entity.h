#ifndef ENTITY_H
#define ENTITY_H

#include <engine/collision.h>
#include <engine/transform.h>

// if you're making a top-down game, you can still use use the Entity struct
// with process_single_tile_collision(), just ignore the is_on_floor member. 
struct Entity
{
	char				is_on_floor;
	struct CollisionBox		colbox;
	struct Vector2x			velocity;
	struct Vector2x			global_tile_pos;
	struct Vector2x			global_pos;
};

// collision checks are done in a way so that the entity does not clip into a specified tile.
// if it is detected that the entity WOULD collide with the specified tile given the entity's current velocity,
// the entity's velocity is reduced to the distance between the entity and the tile
// in process_entity_tile_collision().

// since process_entity_tile_collision() only checks collision for a single tile, you'll
// want to run a for-loop that runs this function against all tiles on-screen.
void process_entity_tile_collision(struct Entity *p_entity, SPRT *p_tile);

#endif

