#ifndef CAMERA_H
#define CAMERA_H

#include <engine/psx.h>
#include <engine/transform.h>
#include <engine/entity.h>
#include <engine/display.h>

// the job of the camera is to cancel-out the on-screen movement of the
// player as much as possible and keep the player near the center of the
// screen at all times. The camera does NOT affect a player's global
// position. It does this by having a center_focal_point, which is the
// center of the screen. It is consistently trying to bring the
// destination_focal_point from wherever it is located, back to the
// center. The destination_focal_point is usually just the center of the
// player (or anything else) on-screen.
struct Camera
{
	// if camera is_free, the camera's center_focal_point does NOT try to keep
	// up with its destination_focal_point via velocity. A side effect of this is
	// that the player's velocities will affect both its own global position AND
	// its own on-screen position. Since the camera does not have any velocity,
	// the level and all other game objects should NOT move unless they are entities
	// with their own velocities.
	char			is_free;

	// smoothing is how loosely the center_focal_point tries to catch up to the
	// destination_focal_point. the lower the number, the more closely it tries
	// to catch up.
	char			smoothing;

	// the camera's velocity is what every other game object will move opposite to,
	// except for the player. It is the velocity used by the center_focal_point to
	// catch up to the destination_focal_point.
	struct Vector2x		velocity;

	// global_pos and global_tile_pos are useful in draw_section() and move_section() in level.c.
	// they help distinguish where the rendering area is!
	struct Vector2x		global_pos;
	struct Vector2x		global_tile_pos;

	struct Vector2x		center_focal_point;		// center of screen
	struct Vector2x		destination_focal_point;	// center of.. anything else (usually the player)

	// toggle drawing of debug members
	char			enable_debug;

	//////// debug members ////////
	LINE_F2			debug_distance;			// distance between center_focal_point and destination_focal_point
	LINE_F2			debug_velocity;			// distance to be traveled next frame
	TILE			debug_center_focal_point;
	TILE			debug_destination_focal_point;
};

void init_camera(struct Camera *p_camera, struct Vector2x *p_section_offset, struct Entity *p_entity);
void process_camera(struct Camera *p_camera, struct Entity *p_entity); // call this before calling move_section or draw_section()
void draw_camera(struct Camera *p_camera);

#endif

