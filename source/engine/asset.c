#include <engine/asset.h>

// variables used specifically for cbready()
unsigned short current_channel; // the channel obtained by CdGetSector()
unsigned short ID;

void play_XA(struct XAFile *p_xa, int channel)
{
	// in case we are switching to a different XA file, set is_playing to 0
	p_currentXA->is_playing = 0;

	// update p_currentXA
	p_currentXA = p_xa;

	// set is_playing to 1 to current XA file
	p_xa->is_playing = 1;

	// change channel
	p_xa->filter.chan = channel;
	CdControlF(CdlSetfilter, (unsigned char *)&p_xa->filter);

	// read from beginning of XA file
	CdControlF(CdlReadS, (unsigned char *)&p_xa->fp.pos);
}

char cdsetup_XA(struct XAFile *p_xa, char *name, int channel, int file)
{
	if(!CdSearchFile(&p_xa->fp, name))
	{
		return 0; // failure
	}

	// should not be playing by default
	p_xa->is_playing = 0;

	// set the filter (which ADPCM channel is playing)
	p_xa->filter.file = file;
	p_xa->filter.chan = channel;
	CdControl(CdlSetfilter, (unsigned char *)&p_xa->filter, 0);

	return 1; // success
}

void cdsetup_mode(unsigned int mode)
{
	CdControl(CdlSetmode, (unsigned char *)(&mode), 0);
	VSync(3); // prevent CD motor screw-up; see LibOver47.pdf pg.196 for more info
}

void cbready(int status, unsigned char *p_result)
{
	// if an XA channel is returning their data sector
	if(status == CdlDataReady)
	{
		// check its eight long words to see if it's a video sector
		CdGetSector((unsigned long *)data_buffer, 8);

		// remember: data is little-endian
		ID = *((unsigned short *)(data_buffer)); // ID == 0x0160 (352)

		// video sector channel number format = 1CCCCC0000000001
		current_channel = *((unsigned short *)(data_buffer+2));
		current_channel = (current_channel>>10) & 31;

		// debug
		if(ID != 0)
		{
			printf("current channel == 0x%hx\nID == 0x%hx\n", current_channel, ID);

			printf("~~\n");
			// %lx is unsigned long
			printf("%lx\n", *(unsigned long *)(data_buffer));
			printf("%lx\n", *(unsigned long *)(data_buffer+8));
			printf("%lx\n", *(unsigned long *)(data_buffer+16));
			printf("%lx\n", *(unsigned long *)(data_buffer+24));
			printf("%lx\n", *(unsigned long *)(data_buffer+32));
			printf("%lx\n", *(unsigned long *)(data_buffer+48));
			printf("%lx\n", *(unsigned long *)(data_buffer+60));
			printf("%lx\n", *(unsigned long *)(data_buffer+72));
			printf("~~\n");
		}

		// if this is a video sector, and if this is the
		// channel being played, replay XA from beginning
		if( (ID == 352) && (current_channel == p_currentXA->filter.chan) )
		{
			CdControl(CdlReadS, (unsigned char *)&p_currentXA->fp.pos, 0);
		}

	}
}

