#include <engine/allocmem.h>

#define ALLOCBUF_SIZE		128000 // change to whatever you want
#define MAX_ALLOCATIONS		150

static char allocbuf[ALLOCBUF_SIZE];

//  
//  This is how allocbuf should look upon program start:
//  
//  ________________________________________________
// |                                                |
// |                 ALLOCBUF_SIZE                  |
// |________________________________________________|
//  <---------------------free--------------------->

static unsigned int alloctable[2][MAX_ALLOCATIONS];

//                   EXAMPLE alloctable
//                   ~~~~~~~~~~~~~~~~~~
//
//                           0      1                    13     14
//                         _________________________________________
//                        |      |      |      |      |      |      |
//   MEMORY ADDRESS     0 | 0x03 | 0x80 |             | NULL | NULL |
//                        |______|______|____ ... ____|______|______|
//                        |      |      |             |      |      |
//        SIZE          1 |  125 |  400 |             | NULL | NULL |
//                        |______|______|______|______|______|______|

unsigned int determine_allocs_used();

void init_allocmem_system(void)
{
	int row, col;

	// set all elements of allocbuf to zero
	for(col = 0; col < ALLOCBUF_SIZE; col++)
	{
		allocbuf[col] = 0;
	}

	// set all elements of alloctable to NULL
	for(row = 0; row < 2; row++)
	{
		for(col = 0; col < MAX_ALLOCATIONS; col++)
		{
			alloctable[row][col] = 0;
		}
	}
}

char *allocmem(unsigned int nobj, unsigned int size)
{
	// nobj -- number of objects
	// size -- size of each object

	int i, j;
	int allocs_used = 0;

	allocs_used = determine_allocs_used();

	for(i = 0; i < MAX_ALLOCATIONS; i++)
	{
		// if the memory address is NULL, then we are to the right
		// of all allocated pieces of memory specified by alloctable.
		if(alloctable[0][i] == (unsigned int)NULL)
		{
			// the first allocation always starts at allocbuf
			if(i == 0)
			{
				// make sure that the requested size does not exceed the upper bound of
				// allocbuf
				if(nobj*size > ALLOCBUF_SIZE)
				{
					return NULL;
				}

				alloctable[0][i] = (unsigned int)allocbuf;
				alloctable[1][i] = nobj*size;
				return (char *)alloctable[0][i];
			}

			// take the end position of the furthest allocated piece
			// of memory, add the requested size from there, and see if
			// it ends before the upper bound of allocbuf. if that is true,
			// use that as the start position of the requested allocated
			// piece of memory. if not, return NULL.
			else if( ((alloctable[0][i-1] + alloctable[1][i-1]) + nobj*size) < (unsigned int)allocbuf + ALLOCBUF_SIZE)
			{
				// append data to the end of the table
				alloctable[0][i] = alloctable[0][i-1] + alloctable[1][i-1];
				alloctable[1][i] = nobj*size;

				return (char *)alloctable[0][i];
			}
			else
			{
				return NULL;
			}
		}

		// we are NOT at the end of the table. since we are in the middle of the table,
		// try to find free space between pieces of already allocated memory. if the
		// requested size fits in between any of these spaces, ensure that all memory
		// addresses are IN ORDER from least to greatest within the table itself. this
		// is done by shifting elements to the right.

		else
		{
			// find a possible space for the new allocation
			if(allocs_used < MAX_ALLOCATIONS)
			{
				// use the next column because it's NULL! it doesn't house a piece of
				// already allocated memory, so see if we can use it.
				if(alloctable[0][i+1] == (unsigned int)NULL)
				{
					continue;
				}

				// otherwise, the next column contains a piece of already allocated memory,
				// and we must see if there is enough free space between the two allocations
				// that will fit the size of our requested allocation.
				else if( ((alloctable[0][i+1]) - (alloctable[0][i] + alloctable[1][i])) >= nobj*size)
				{

					// create space for the new allocation by shifting everything after
					// column "i" forward starting at the end of alloctable going
					// backwards.
					for(j = allocs_used-1; j > i; j--)
					{
						alloctable[0][j+1] = alloctable[0][j];
						alloctable[1][j+1] = alloctable[1][j];
					}

					// insert the data!
					alloctable[0][i+1] = alloctable[0][i] + alloctable[1][i];
					alloctable[1][i+1] = nobj*size;

					// return a pointer to the address
					return (char *)alloctable[0][i+1];
				}
			}
			// all allocation pieces have been used!
			else
			{
				return NULL;
			}
		}
	}

	// we have ultimately failed to find any free space for the requested
	// allocation, so return NULL.
	return NULL;
}

void freemem(char *p_addr)
{
	int i, j;
	int allocs_used;

	allocs_used = determine_allocs_used();

	for(i = 0; i < MAX_ALLOCATIONS; i++)
	{
		if(alloctable[0][i] == (unsigned int)p_addr)
		{
			// if the element is at the end of the table, then set it to zero.
			// no shifting required.
			if(i == allocs_used-1)
			{
				alloctable[0][i] = 0;
				alloctable[1][i] = 0;

				return;
			}

			// if the element to be freed is before the end of the table, we must
			// shift elements to the left starting at column "i" going forward.
			else if(i < allocs_used-1)
			{
				for(j = i+1; j < allocs_used; j++)
				{
					alloctable[0][j-1] = alloctable[0][j];
					alloctable[1][j-1] = alloctable[1][j];

					alloctable[0][j] = 0;
					alloctable[1][j] = 0;
				}

				return;
			}
		}
	}
}

void seemem(void)
{
	unsigned int col, space_used = 0;

	// print "[COLUMN] MEMORY ADDRESS : SIZE"
	for(col = 0; col < MAX_ALLOCATIONS; col++)
	{
		printf("[%u] 0x%x : %u\n", col, alloctable[0][col], alloctable[1][col]);

		space_used += alloctable[1][col];
	}

	printf("\nspace used: %u/%u bytes\n", space_used, ALLOCBUF_SIZE);
}

unsigned int determine_allocs_used()
{
	int i, allocs_used = 0;

	// how many memory allocations has the program made?
	for(i = 0; i < MAX_ALLOCATIONS; i++)
	{
		if(alloctable[0][i] == (unsigned int)NULL)
		{
			return allocs_used;
		}

		allocs_used++;
	}

	return allocs_used;
}

