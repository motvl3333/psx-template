#include <engine/anim.h>
#include <engine/level.h>

void process_animated_sprite(struct AnimatedSprite *p_sprite, short specified_animation, char speed)
{
	// for brevity
	unsigned short		*p_current_frame  =	&p_sprite->current_frame;
	unsigned short		*p_frame_count    =	&p_sprite->p_animation[specified_animation].frame_count;
	struct UCharRECT	*p_frames         =	p_sprite->p_animation[specified_animation].p_frames; // p_frames is already a pointer

	int			i;

	if(speed == 0)
	{
		return;
	}

	if(p_sprite->is_playing && !(VSync(-1) % speed))
	{
		// since an animation with a higher frame_count can suddenly be switched
		// to an animation with a lower frame_count, use >= instead of == here
		if(*p_current_frame >= *p_frame_count-1)
		{
			*p_current_frame = 0;
		}
		else
		{
			*p_current_frame += 1;
		}
	}

	// update UV values based on current frame

	// WITHIN THE TEXTURE PAGE:
	//
	//  (u0, v0)               (u1, v1)
	//        ____________________
	//       |                    |
	//       |                    |
	//       |                    |
	//       |                    |
	//       |                    |
	//       |                    |
	//       |                    |
	//       |____________________|
	//
	//  (u2, v2)               (u3, v3)

	// set (u, v) values
	p_sprite->poly_ft4.u0 = p_sprite->poly_ft4.u2 = p_frames[*p_current_frame].x;
	p_sprite->poly_ft4.u1 = p_sprite->poly_ft4.u3 = p_frames[*p_current_frame].x + p_frames[*p_current_frame].w;

	p_sprite->poly_ft4.v0 = p_sprite->poly_ft4.v1 = p_frames[*p_current_frame].y;
	p_sprite->poly_ft4.v2 = p_sprite->poly_ft4.v3 = p_frames[*p_current_frame].y + p_frames[*p_current_frame].h;
}

void free_animated_sprite(struct AnimatedSprite *p_sprite)
{
	int i;

	// first, free the frames allocated by each animation
	for(i = 0; i < p_sprite->animation_count; i++)
	{
		freemem((char *)p_sprite->p_animation[i].p_frames);
	}

	// now free the space for the animation pointer itself
	freemem((char *)p_sprite->p_animation);
}

#define INIT_COMMON_SPRITE_VALUES(p_sprite, clut, tpage, i, x, y, w, h, u, v, uw, vh, region) \
	(p_sprite)->deg = 4096 * 225;					\
	(p_sprite)->radius = 16;					\
	(p_sprite)->h_is_flipped = 0;					\
	(p_sprite)->v_is_flipped = 0;					\
	(p_sprite)->enable_debug = 0;					\
									\
	SetPolyFT4(&((p_sprite)->poly_ft4));				\
	setRGB0(&((p_sprite)->poly_ft4), 128, 128, 128);		\
	((p_sprite)->poly_ft4).clut = clut;				\
	((p_sprite)->poly_ft4).tpage = tpage;				\
									\
	((p_sprite)->poly_ft4).x0 = ((p_sprite)->poly_ft4).x2 = x;	\
	((p_sprite)->poly_ft4).x1 = ((p_sprite)->poly_ft4).x3 = x + w;	\
	((p_sprite)->poly_ft4).y0 = ((p_sprite)->poly_ft4).y1 = y;	\
	((p_sprite)->poly_ft4).y2 = ((p_sprite)->poly_ft4).y3 = y + h;	\
									\
	((p_sprite)->poly_ft4).u0 = ((p_sprite)->poly_ft4).u2 = u;	\
	((p_sprite)->poly_ft4).u1 = ((p_sprite)->poly_ft4).u3 = u + uw;	\
	((p_sprite)->poly_ft4).v0 = ((p_sprite)->poly_ft4).v1 = v;	\
	((p_sprite)->poly_ft4).v2 = ((p_sprite)->poly_ft4).v3 = v + vh;	\
									\
	((p_sprite)->center).x = (x + (x+w))/2;				\
	((p_sprite)->center).y = (y + (y+h))/2;				\
									\
	for(i = 0; i < SECOND * 2; i++)					\
	{								\
		SetTile1(&((p_sprite)->debug_circle[i]));		\
									\
		setRGB0(&((p_sprite)->debug_circle[i]), 0, 0, 0);	\
		if(region == NTSC_DISC)					\
		{							\
			((p_sprite)->debug_circle[i]).x0 = ((p_sprite)->center).x + NTSC_x_coords[i]; \
			((p_sprite)->debug_circle[i]).y0 = ((p_sprite)->center).y + NTSC_y_coords[i]; \
		}					        	\
		else					        	\
		{					        	\
			((p_sprite)->debug_circle[i]).x0 = ((p_sprite)->center).x + PAL_x_coords[i]; \
			((p_sprite)->debug_circle[i]).y0 = ((p_sprite)->center).y + PAL_y_coords[i]; \
		}							\
	}								\
									\
	SetTile(&((p_sprite)->debug_center));				\
	setRGB0(&((p_sprite)->debug_center), 0, 0, 0);			\
	((p_sprite)->debug_center).w = ((p_sprite)->debug_center).h = 2;\
	((p_sprite)->debug_center).x0 = ((p_sprite)->center).x - 1;	\
	((p_sprite)->debug_center).y0 = ((p_sprite)->center).y - 1

char init_sprite(void *p_sprite_pointer, enum SpriteType type, unsigned short animation_count, struct ShortRECT *p_screen_pos, struct UCharRECT *p_texpage_pos, unsigned short tpage, unsigned short clut)
{
	int			i;

	short			x, y, w, h;
	unsigned char		u, v, uw, vh;

	struct Sprite		*p_sprite     = p_sprite_pointer;
	struct AnimatedSprite	*p_animsprite = p_sprite_pointer;

	x = p_screen_pos->x;
	y = p_screen_pos->y;
	w = p_screen_pos->w;
	h = p_screen_pos->h;

	u = p_texpage_pos->x;
	v = p_texpage_pos->y;
	uw = p_texpage_pos->w;
	vh = p_texpage_pos->h;

	// dereference the pointer with the correct type
	switch(type)
	{
		case SPRITE:
			INIT_COMMON_SPRITE_VALUES(p_sprite, clut, tpage, i, x, y, w, h, u, v, uw, vh, REGION);
			break;
		case ANIMATED_SPRITE:
			INIT_COMMON_SPRITE_VALUES(p_animsprite, clut, tpage, i, x, y, w, h, u, v, uw, vh, REGION);

			// init all AnimatedSprite-specific stuff here
			p_animsprite->animation_count = animation_count;
			p_animsprite->current_frame = 0;
			p_animsprite->is_playing = 0;

			// allocate as many animations as specified. please note that you
			// have to initialize the Animation structs manually!
			p_animsprite->p_animation = (struct Animation *)allocmem(animation_count, sizeof(struct Animation));

			if(!p_animsprite->p_animation)
			{
				printf("failed to allocate memory for AnimatedSprite!\n");
				seemem();
				return 0; // failure
			}
			break;
	}

	return 1; // success
}

void init_animation(struct Animation *p_animation,
			unsigned char texture_column_start,	// u start value in pixels
			unsigned char texture_row_start,	// v start value in pixels
			unsigned char width,		// width of each frame
			unsigned char height,		// height of each frame
			unsigned short frame_count)
{
	int i, j;
	int left_side_of_current_frame; 
	int right_side_of_texture = 255;

	p_animation->frame_count = frame_count;

	// in the for-loop below, "j" helps determine the correct position of each frame while 
	// "i" specifies the correct frame index in the animation structure. "j" initially acts
	// as the same as "i", but "j" can be reset back to 0 while "i" cannot.
	for(i = 0, j = 0; i < frame_count; i++, j++)
	{
		left_side_of_current_frame = (width + TILE_SPACING)*j;

		// if a would-be frame extends beyond the right side of the texture page
		if( (left_side_of_current_frame + width) > right_side_of_texture)
		{
			// move "u" to the left of the texture
			texture_column_start = 1;
			j = 0;

			// move "v" one row down
			texture_row_start += height + TILE_SPACING;
		}

		// set actual values
		p_animation->p_frames[i].x = (width + TILE_SPACING)*j + texture_column_start;
		p_animation->p_frames[i].y = (texture_row_start);

		p_animation->p_frames[i].w = width;
		p_animation->p_frames[i].h = height;
	}
}

#define MOVE_COMMON_SPRITE_VALUES(p_sprite, i, x, y) \
	MOVE_POLY4(&((p_sprite)->poly_ft4), x, y);	\
							\
	((p_sprite)->center).x += x;			\
	((p_sprite)->center).y += y;			\
							\
	for(i = 0; i < SECOND * 2; i++)			\
	{						\
		((p_sprite)->debug_circle[i]).x0 += x;	\
		((p_sprite)->debug_circle[i]).y0 += y;	\
	}						\
							\
	((p_sprite)->debug_center).x0 += x;		\
	((p_sprite)->debug_center).y0 += y		\

void move_sprite(void *p_sprite_pointer, enum SpriteType type, short x, short y)
{
	int			i;

	struct Sprite		*p_sprite     = p_sprite_pointer;
	struct AnimatedSprite	*p_animsprite = p_sprite_pointer;

	// dereference the pointer with the correct type
	switch(type)
	{
		case SPRITE:
			MOVE_COMMON_SPRITE_VALUES(p_sprite, i, x, y);
			break;
		case ANIMATED_SPRITE:
			MOVE_COMMON_SPRITE_VALUES(p_animsprite, i, x, y);
			break;
	}
}

#define DRAW_COMMON_SPRITE(p_sprite, i) \
	DrawPrim(&((p_sprite)->poly_ft4));		\
							\
	if((p_sprite)->enable_debug)			\
	{						\
		for(i = 0; i < SECOND * 2; i++)		\
		{					\
			DrawPrim(&((p_sprite)->debug_circle[i])); \
		}					\
							\
		DrawPrim(&((p_sprite)->debug_center));	\
	}

void draw_sprite(void *p_sprite_pointer, enum SpriteType type)
{
	int	i;

	struct Sprite			*p_sprite     = p_sprite_pointer;
	struct AnimatedSprite		*p_animsprite = p_sprite_pointer;

	// dereference the pointer with the correct type
	switch(type)
	{
		case SPRITE:
			DRAW_COMMON_SPRITE(p_sprite, i);
			break;
		case ANIMATED_SPRITE:
			DRAW_COMMON_SPRITE(p_animsprite, i);
			break;
	}
}

